<?php
namespace Craft;

class ColorConverterPlugin extends BasePlugin
{
    public function getName()
    {
         return Craft::t('Color Converter');
    }

    public function getVersion()
    {
        return '0.1.0';
    }

    public function getDeveloper()
    {
        return 'Michael Rog';
    }

    public function getDeveloperUrl()
    {
        return 'http://michaelrog.com';
    }

    public function hasCpSection()
    {
        return false;
    }

    public function addTwigExtension()
    {
        Craft::import('plugins.colorconverter.twigextensions.ColorConverterTwigExtension');

        return new ColorConverterTwigExtension();
    }
}
